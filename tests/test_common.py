"""Test the classes and functions defined by gg/common.py"""

from unittest.mock import Mock, call

from tests import BaseTestCase

print_ = Mock()


class CommonTestCase(BaseTestCase):
    filename = 'common'

    def setUp(self):
        super().setUp()
        print_.reset_mock()

    def test_singleton(self):
        """Ensure we can define classes that have only one instance."""
        @self.mod.singleton
        class Highlander:
            pass
        self.assertEqual(Highlander(), Highlander)
        self.assertIs(Highlander(), Highlander)
        self.assertEqual(id(Highlander()), id(Highlander))

    def test_memoize_function(self):
        """Ensure we can define functions that cache return values."""
        @self.mod.memoize
        def doubler(foo):
            print_('Expensive calculation!')
            return foo * 2
        self.assertEqual(doubler(50), 100)
        self.assertEqual(doubler(50), 100)
        print_.assert_called_once_with('Expensive calculation!')  # Once!

    def test_memoize_class(self):
        """Ensure we can define classes that cache instances."""
        @self.mod.memoize
        class Memorable:
            def __init__(self, foo):
                print_('Expensive calculation!')
        self.assertIs(Memorable('alpha'), Memorable('alpha'))
        self.assertIsNot(Memorable('alpha'), Memorable('beta'))
        self.assertEqual(len(Memorable.instances), 2)
        self.assertEqual(print_.call_count, 2)

    def test_binding(self):
        """Ensure we can bind GObject properties to GSettings keys."""
        m = self.mod.GObject.Binding.__init__ = Mock()
        self.mod.Binding('foo', 'bar', 'grill')
        m.assert_called_once_with(
            source='foo', source_property='bar', target='grill',
            target_property='bar', flags=self.mod.GObject.BindingFlags.DEFAULT)

    def test_gsettings_init(self):
        """Ensure GSettings object sets itself up correctly."""
        self.mod.GSettings(schema='wat', path='goober')
        self.assertSequenceEqual(self.mod.Gio.Settings.__init__.mock_calls, [
            call('ca.gottengeography', None),
            call('ca.gottengeography.wat', '/ca/gottengeography/wats/goober/'),
        ])

    def test_gst_set_history(self):
        """Set history value correctly."""
        self.mod.Gst.set_history([5.2, 10.6, 8])
        self.mod.Gst.set_value.assert_called_once_with(
            'history', self.mod.GLib.Variant.return_value)
        self.mod.GLib.Variant.assert_called_once_with(
            'a(ddi)', [5.2, 10.6, 8])

    def test_gst_set_window_size(self):
        """Set window size correctly."""
        self.mod.Gst.set_window_size([800, 600])
        self.mod.Gst.set_value.assert_called_once_with(
            'window-size', self.mod.GLib.Variant.return_value)
        self.mod.GLib.Variant.assert_called_once_with(
            '(ii)', [800, 600])

    def test_gst_set_color(self):
        """Set color correctly."""
        color = Mock(red=1, green=2, blue=3)
        self.mod.Gst.set_color(color)
        self.mod.Gst.set_value.assert_called_once_with(
            'track-color', self.mod.GLib.Variant.return_value)
        self.mod.GLib.Variant.assert_called_once_with(
            '(iii)', (1, 2, 3))
