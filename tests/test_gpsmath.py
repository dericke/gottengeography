"""Test the classes and functions defined by gg/gpsmath.py"""

from tests import BaseTestCase


class GpsmathTestCase(BaseTestCase):
    filename = 'gpsmath'

    def test_valid_coords(self):
        """Ensure we can identify valid coords."""
        valid = (
            (0, 0),
            (50, 50),
            (90, 180),
            (1.1, 1.2),
            (-90.0, -180.0),
        )
        result = [self.mod.valid_coords(*coord) for coord in valid]
        assert False not in result, result
        invalid = (
            ({}, 0),
            (50, 'huh'),
            (90.001, 180),
            (1.1, 180.00000001),
        )
        result = [self.mod.valid_coords(*coord) for coord in invalid]
        assert True not in result, result

    def test_do_cached_lookup(self):
        """Rough coord lookup."""
        result = self.mod.do_cached_lookup(self.mod.GeoCacheKey(50, -113))
        self.assertEqual(
            result, ('Picture Butte', '01', 'CA', 'America/Edmonton\n'))

    def test_geocachekey_str(self):
        """What is the string representation of a GeoCacheKey?"""
        self.assertEqual(
            str(self.mod.GeoCacheKey(1.23456, -10.234567)), '1.23,-10.23')

    def test_geocachekey_eq(self):
        """Do GeoCacheKey roughly equal?"""
        self.assertEqual(
            self.mod.GeoCacheKey(1.23456, -10.234567),
            self.mod.GeoCacheKey(1.23499, -10.234999))
        self.assertEqual(
            self.mod.GeoCacheKey(1.23456, -10.234567),
            self.mod.GeoCacheKey(1.22600, -10.226000))
