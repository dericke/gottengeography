# Bulgarian translation for gottengeography
# Copyright (c) 2012 Rosetta Contributors and Canonical Ltd 2012
# This file is distributed under the same license as the gottengeography package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: gottengeography\n"
"Report-Msgid-Bugs-To: Robert Bruce Park <robru@gottengeography.ca>\n"
"POT-Creation-Date: 2012-08-16 22:37-0500\n"
"PO-Revision-Date: 2012-10-22 10:46+0000\n"
"Last-Translator: Robert Bruce Park <robert.park@canonical.com>\n"
"Language-Team: Bulgarian <bg@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2012-10-23 05:19+0000\n"
"X-Generator: Launchpad (build 16179)\n"

#: ../data/gottengeography.ui.h:1
msgid "_Open..."
msgstr "_Отваряне..."

#: ../data/gottengeography.ui.h:2
msgid "_Save All"
msgstr "_Запис на всички"

#: ../data/gottengeography.ui.h:3
msgid "_Help"
msgstr "_Помощ"

#: ../data/gottengeography.ui.h:4
msgid "_About"
msgstr "_За програмата"

#: ../data/gottengeography.ui.h:5
msgid "_Quit"
msgstr "_Изход"

#: ../data/gottengeography.ui.h:6
msgid "Load photos or GPS data (Ctrl O)"
msgstr "Зареждане на фотографии или на данни от GPS устройство (Ctrl O)"

#: ../data/gottengeography.ui.h:7
msgid "Change map view (Google opens in your browser)"
msgstr "Промяна на изгледа на картата (Google се отваря в уеббраузъра ви)"

#: ../data/gottengeography.ui.h:8
msgid "Return to the previous location viewed on map (Ctrl Left)"
msgstr ""
"Връщане към предишното местоположение, прегледано на картата (Ctrl Left)"

#: ../data/gottengeography.ui.h:9
msgid "Type the name of the city you would like to view."
msgstr "Моля, въведете името на града, който бихте искали да разгледате."

#: ../data/gottengeography.ui.h:10
msgid "Zoom the map out one step (Ctrl -)"
msgstr "Умаляване на картата с една стъпка (Ctrl -)"

#: ../data/gottengeography.ui.h:11
msgid "Enhance! (Ctrl +)"
msgstr "Увеличение (Ctrl +)"

#: ../data/gottengeography.ui.h:12
msgid "Learn how to use GottenGeography (Ctrl ?)"
msgstr "Научете как да използвате GottenGeography (Ctrl ?)"

#: ../data/gottengeography.ui.h:13
msgid "About GottenGeography"
msgstr "За GottenGeography"

#: ../data/gottengeography.ui.h:14
msgid "Drag files here to load."
msgstr "С мишката довлачете до и пуснете файлове тук, за да ги заредите."

#: ../data/gottengeography.ui.h:15
msgid "Close selected photos (Ctrl W)"
msgstr "Затваряне на избраните фотографии (Ctrl W)"

#: ../data/gottengeography.ui.h:16
msgid "Reload selected photos, losing all changes (Ctrl Z)"
msgstr ""
"Презареждане на избраните фотографии, като така губите всички промени по тях "
"(Ctrl Z)"

#: ../data/gottengeography.ui.h:17
msgid "Center the map view on the selected photo (Ctrl J)"
msgstr "Центриране на изгледа на картата върху избраната фотография (Ctrl J)"

#: ../data/gottengeography.ui.h:18
msgid "Save all photos (Ctrl S)"
msgstr "Запис на всички фотографии (Ctrl S)"

#: ../data/gottengeography.ui.h:19
msgid "Save All"
msgstr "Запис на всичко"

#: ../data/gottengeography.ui.h:20
msgid "Place selected photos onto center of map (Ctrl Enter)"
msgstr ""
"Поставяне на избраните фотографии върху центъра на картата (Ctrl Enter)"

#: ../data/gottengeography.ui.h:21
msgid "Photos"
msgstr "Фотографии"

#: ../data/gottengeography.ui.h:22
msgid "Cameras"
msgstr "Камери"

#: ../data/gottengeography.ui.h:23
msgid "GPS"
msgstr "GPS"

#: ../data/gottengeography.ui.h:24
msgid "Is this a photo of a clock? Type the displayed time here:"
msgstr "Това снимка на чаовник ли е? Въведете показаното време тук:"

#: ../data/gottengeography.ui.h:25
msgid "Set Clock Offset!"
msgstr "Задайте часови пояс!"

#: ../data/gottengeography.ui.h:26
msgid "Open Files"
msgstr "Отваряне на файлове"

#: ../data/gottengeography.ui.h:27
msgid ""
"GottenGeography: Go open thy tagger!\n"
"\n"
"This program is written in the Python programming language, and adds geotags "
"to your photos. The name is an anagram of \"Python Geotagger.\""
msgstr ""
"GottenGeography: Да отворим таговете!\n"
"\n"
"Тази програма е написана на езика Python и добавя гео локация към Вашите "
"снимки. Името е анаграма на \"Python Geotagger\"."

#: ../data/gottengeography.ui.h:30
msgid "GottenGeography Wiki"
msgstr "GottenGeography Wiki"

#: ../data/gottengeography.ui.h:31
msgid "translator-credits"
msgstr ""
"Launchpad Contributions:\n"
"  Robert Bruce Park https://launchpad.net/~robru\n"
"  Ve4ernik https://launchpad.net/~ve4ernik\n"
"  spacy01 https://launchpad.net/~spacy00001"

#: ../data/gottengeography.ui.h:32
msgid ""
"<span weight=\"bold\" size=\"larger\">Save changes to your photos before "
"closing?</span>"
msgstr ""
"<span weight=\"bold\" size=\"larger\">Запазване на промените преди "
"затваряне?</span>"

#: ../data/gottengeography.ui.h:34
#, no-c-format
msgid ""
"The changes you've made to %d of your photos will be permanently lost if you "
"do not save."
msgstr ""
"Промените, които направихте на %d ще бъдат загубени ако не ги запазите."

#: ../data/gottengeography.ui.h:35
msgid "Close _without Saving"
msgstr "Затваряне _без запазване"

#: ../data/camera.ui.h:1
msgid "Use the system timezone."
msgstr "Използване на системния часовник."

#: ../data/camera.ui.h:2
msgid "Use the local timezone."
msgstr "Използване на локален  часови пояс."

#: ../data/camera.ui.h:3
msgid "Specify a UTC offset manually."
msgstr "Ръчна настройкаспоред UTC."

#: ../data/camera.ui.h:4
msgid "Specify the timezone manually."
msgstr "Ръчна настройка на часови пояс."

#: ../gg/app.py:179
msgid "Could not open: "
msgstr "Не може да се отвори: "

#: ../gg/gpsmath.py:211
msgid "N"
msgstr "С"

#: ../gg/gpsmath.py:211
msgid "S"
msgstr "Ю"

#: ../gg/gpsmath.py:212
msgid "E"
msgstr "И"

#: ../gg/gpsmath.py:212
msgid "W"
msgstr "З"

#: ../gg/gpsmath.py:219
msgid "m above sea level"
msgstr "м над морското равнище"

#: ../gg/gpsmath.py:220
msgid "m below sea level"
msgstr "м под морското равнище"

#: ../gg/camera.py:92
msgid "Unknown Camera"
msgstr "Неизвестна камера"

#: ../gg/camera.py:222
#, python-format
msgid "Add %dm, %ds to clock."
msgstr "Добавяне на %dm, %ds към часовника."

#: ../gg/camera.py:223
#, python-format
msgid "Subtract %dm, %ds from clock."
msgstr "Премахне на %dm, %ds от часовника."

#: ../gg/camera.py:287
msgid "No photos loaded."
msgstr "Няма заредени фотографии."

#: ../gg/camera.py:288
msgid "One photo loaded."
msgstr "Една фотография е заредена."

#: ../gg/camera.py:289
#, python-format
msgid "%d photos loaded."
msgstr "%d  фотографии са заредени."

#: ../gg/xmlfiles.py:210
#, python-format
msgid "%d points loaded in %.2fs."
msgstr "%d точки заредени в %.2fs."
